<?php
class main
{
  /* json files model data structure variable
   containing events timestamp */
  public $times = [
    'minute' => '',
    '1hour' => '',
    '4hour' => '',
    'week' => '',
    'mounth' => ''
  ];

  public function  __construct()
  {
    $this->timeFileURI = 'include/files/time.json';
    $this->times = (array)json_decode(file_get_contents($this->timeFileURI));
  }

  public function isTimeToUpdateTimeMinute()
  {
    if(($this->times['minute'] + 60) <= time()) return true;

    return false;
  }

  public function isTimeToUpdateTime1Hour()
  {
    if(($this->times['1hour'] + 3600) <= time()) return true;

    return false;
  }

  public function isTimeToUpdateTime4hour()
  {
    if(($this->times['4hour'] + 14400) <= time()) return true;

    return false;
  }

  public function isTimeToUpdateTimeWeek()
  {
    if(($this->times['week'] + 604800) <= time()) return true;

    return false;
  }

  public function isTimeToUpdateTimeMounth()
  {
    if(($this->times['mounth'] + 2419200) <= time()) return true;

    return false;
  }

  public function putTime($index)
  {
    $arrayTime[$index] = time();
    $putReturn = file_put_contents($this->timeFileURI,json_encode($arrayTime));

    if(is_bool($putReturn)) return false;

    return true;
  }

  public function getTimeMinute()
  {
    return $this->times['minute'];
  }

  public function getTime1Hour()
  {
    return $this->times['1hour'];
  }

  public function getTime4hour()
  {
    return $this->times['4hour'];
  }

  public function getTimeWeek()
  {
    return $this->times['week'];
  }

  public function getTimeMounth()
  {
    return $this->times['mounth'];
  }
}
?>
